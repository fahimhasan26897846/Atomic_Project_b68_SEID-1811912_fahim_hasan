<?php $__env->startSection('title','Gender - Create Form'); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Create Form</h3>
            <hr>

            <?php echo Form::open(['url'=>'/Gender/store'] , ['class'=>'form-horizontal']); ?>


            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name','', ['class'=>'form-control', 'required'=>'required']); ?>

            <br>

            <?php echo Form::label('gender','Gender:'); ?><br>
            <?php echo Form::label('gender','male:'); ?>

            <?php echo Form::radio('gender', 'male'); ?><br>
            <?php echo Form::label('gender','Female:'); ?>

            <?php echo Form::radio('gender', 'female'); ?><br>
            <?php echo Form::label('gender','Other:'); ?>

            <?php echo Form::radio('gender','other'); ?>


            <br>

            <?php echo Form::submit('Submit',['class'=> 'btn btn-success']); ?>


            <?php echo Form::close(); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>