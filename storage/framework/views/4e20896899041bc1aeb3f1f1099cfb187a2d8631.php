<?php $__env->startSection('title','Hobbies - Edit Form'); ?>




<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Edit Form</h3>
            <hr>

    <?php echo Form::open(['url'=>'/Hobbies/update']); ?>


            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']); ?>


            <br>

            <?php echo Form::label('hobbies','Hobbies:'); ?><br>

            <?php echo Form::label('hobbies','Sleeping:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'sleeping' ); ?><br>
            <?php echo Form::label('hobbies','Php coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'Php coding' ); ?><br>
            <?php echo Form::label('hobbies','C coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'C coding' ); ?><br>
            <?php echo Form::label('hobbies','Python coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'Python coding' ); ?><br>
            <?php echo Form::label('hobbies','Circuit solution:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'Circuit solution' ); ?>

            <br>

     <?php echo Form::text('id',$oneData['id'],['hidden'=>'hidden']); ?>


    <?php echo Form::submit('Update',['class'=> 'btn btn-success']); ?>


    <?php echo Form::close(); ?>


    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>