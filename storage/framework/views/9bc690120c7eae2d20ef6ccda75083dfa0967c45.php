<?php $__env->startSection('title','City - Create Form'); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Create Form</h3>
            <hr>

            <?php echo Form::open(['url'=>'/City/store','class'=>'form-horizontal']); ?>


            <?php echo Form::label('city','City:'); ?>

            <?php echo Form::select('city', ['dhaka' => 'dhaka', 'comilla' => 'comilla','noakhali' => 'noakhali','chittagong' => 'chittagong','borishal' => 'borishal'],'',['class'=>'form-control']); ?>


            <br>

            <?php echo Form::submit('Submit',['class'=> 'btn btn-success']); ?>


            <?php echo Form::close(); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>