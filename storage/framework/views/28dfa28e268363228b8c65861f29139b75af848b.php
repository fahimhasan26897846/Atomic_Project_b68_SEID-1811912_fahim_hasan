<?php $__env->startSection('title','Summary of organization - Active List'); ?>


<?php $__env->startSection('content'); ?>


    <div class="container">

        <div class="navbar">

            <a href="/summary_of_organization"><button type="button" class="btn btn-primary">Add New</button></a>
        </div>

        <?php echo Form::open(['url'=>'Summary_Of_Organization/search_result']); ?>



        <?php echo Form::text('keyword'); ?>

        <?php echo Form::submit('Search',['class'=> 'btn btn-success']); ?>


        <?php echo Form::close(); ?>





        Total: <?php echo $allData->total(); ?> Summary(s) <br>

        Showing: <?php echo $allData->count(); ?> Summary(s) <br>

        <?php echo $allData->links(); ?>





        <table class="table table-bordered table table-striped" >

            <th>name</th>
            <th>summary</th>

            <th>Action Buttons</th>

            <?php $__currentLoopData = $allData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $oneData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <tr>

                    <td>  <?php echo $oneData['name']; ?> </td>
                    <td>  <?php echo $oneData['summary']; ?> </td>


                    <td>
                        <a href="view/<?php echo $oneData['id']; ?>"><button class="btn btn-info">View</button></a>
                        <a href="edit/<?php echo $oneData['id']; ?>"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/<?php echo $oneData['id']; ?>"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        </table>
        <?php echo $allData->links(); ?>

    </div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>