<?php $__env->startSection('title','city - Edit Form'); ?>




<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Edit Form</h3>
            <hr>
    <?php echo Form::open(['url'=>'/City/update']); ?>


    <?php echo Form::label('city','city:'); ?>

            <?php echo Form::select('city', ['dhaka' => 'dhaka', 'comilla'=> 'comilla','noakhali' => 'noakhali','chittagong' => 'chittagong','borishal' => 'borishal'],'',['class'=>'form-control']); ?>

    <br>

     <?php echo Form::text('id',$oneData['id'],['hidden'=>'hidden']); ?>


    <?php echo Form::submit('Update',['class'=> 'btn btn-success']); ?>


    <?php echo Form::close(); ?>


    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>