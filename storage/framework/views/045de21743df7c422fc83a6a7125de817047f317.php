<?php $__env->startSection('title','PP - Edit Form'); ?>




<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> PP - Edit Form</h3>
            <hr>

    <?php echo Form::open(['url'=>'/Profile_Picture/update']); ?>


            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']); ?>


            <br>

            <?php echo Form::label('profile_picture','Profile Picture:'); ?>

            <?php echo Form::file('profile_picture'); ?>

            <br>

            <?php echo Form::text('id',$oneData['id'],['hidden'=>'hidden']); ?>


    <?php echo Form::submit('Update',['class'=> 'btn btn-success']); ?>


    <?php echo Form::close(); ?>


    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>