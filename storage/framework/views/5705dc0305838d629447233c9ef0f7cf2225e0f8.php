<?php $__env->startSection('title','Hobbies - Create Form'); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Create Form</h3>
            <hr>

            <?php echo Form::open(['url'=>'/Hobbies/store']); ?>


            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name','',['class'=>'form-control', 'required'=>'required']); ?>


            <br>

            <?php echo Form::label('hobbies','Hobbies:'); ?><br>

            <?php echo Form::label('hobbies','Sleeping:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'sleeping' ); ?><br>
            <?php echo Form::label('hobbies','Php coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'Php coding' ); ?><br>
            <?php echo Form::label('hobbies','Python coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'C coding' ); ?><br>
            <?php echo Form::label('hobbies','Python coding:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'C coding' ); ?><br>
            <?php echo Form::label('hobbies','Circuit solution:'); ?>

            <?php echo Form::checkbox('hobbies[]', 'Circuit solution' ); ?>

            <br>

            <?php echo Form::submit('Submit',['class'=> 'btn btn-success']); ?>


            <?php echo Form::close(); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>