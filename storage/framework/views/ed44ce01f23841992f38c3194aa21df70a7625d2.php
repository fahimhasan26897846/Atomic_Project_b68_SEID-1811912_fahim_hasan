<?php $__env->startSection('title','Book Title - Single Book'); ?>


<?php $__env->startSection('content'); ?>


    <h1> Single Book Information: </h1>
    <table class="table table-bordered">

       <tr> <td> Book Title</td> <td> <?php echo $oneData['book_title']; ?> </td> </tr>
        <tr> <td> Author Name</td> <td><?php echo $oneData['author_name']; ?> </td> </tr>

     </table>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>