<?php $__env->startSection('title','Email - Create Form'); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Email - Create Form</h3>
            <hr>

            <?php echo Form::open(['url'=>'/Email/store']); ?>


            <?php echo Form::label('name','Name:'); ?>

            <?php echo Form::text('name','', ['class'=>'form-control', 'required'=>'required']); ?>

            <br>

            <?php echo Form::label('email','email:'); ?>

            <?php echo Form::email('email','',['class'=>'form-control', 'required'=>'required']); ?>


            <br>

            <?php echo Form::submit('Submit',['class'=> 'btn btn-success']); ?>


            <?php echo Form::close(); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>