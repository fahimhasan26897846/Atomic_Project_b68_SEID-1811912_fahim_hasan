<?php $__env->startSection('title','Book Title - Create Form'); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Book Title - Create Form</h3>
            <hr>

            <?php echo Form::open(['url'=>'/Book_Title/store']); ?>


            <?php echo Form::label('book_title','Book Title:'); ?>

            <?php echo Form::text('book_title','',['class'=>'form-control', 'required'=>'required']); ?>


            <br>

            <?php echo Form::label('author_name','Author Name:'); ?>

            <?php echo Form::text('author_name','', ['class'=>'form-control', 'required'=>'required']); ?>

            <br>

            <?php echo Form::submit('Submit',['class'=> 'btn btn-success']); ?>


            <?php echo Form::close(); ?>


        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('../master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>