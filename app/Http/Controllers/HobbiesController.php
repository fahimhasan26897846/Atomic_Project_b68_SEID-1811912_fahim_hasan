<?php

namespace App\Http\Controllers;

use App\Hobbies;
use Illuminate\Http\Request;

class HobbiesController extends Controller
{
    public function store(){

        $objModel = new Hobbies();
        $objModel->name=$_POST['name'];
        $strHobbies = implode(",",$_POST["hobbies"]);
        $_POST["hobbies"] = $strHobbies;
        $objModel->hobbies=$_POST["hobbies"];
        $status=$objModel->save();
        return redirect()->route('HobbiesCreate');
    }
    public function index(){

        $objHobbiesModel = new Hobbies();

        $allData = $objHobbiesModel->paginate(5);


        return view("Hobbies/index",compact('allData'));

    }



    public function view($id){


        $objHobbiesModel = new Hobbies();


        $oneData = $objHobbiesModel->find($id);

        return view('Hobbies/view',compact('oneData'));

    }




    public function edit($id){


        $objHobbiesModel = new Hobbies();

        $oneData = $objHobbiesModel->find($id);

        return view('Hobbies/edit',compact('oneData'));
    }




    public function update(){


        $objHobbiesModel = new Hobbies();
        $strHobbies = implode(",",$_POST["hobbies"]);
        $_POST["hobbies"] = $strHobbies;
        $oneData = $objHobbiesModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->hobbies = $_POST["hobbies"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('hobbiesindex');


    }



    public function delete($id){


        $objHobbiesModel = new Hobbies();

        $status = $objHobbiesModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('hobbiesindex');

    }


    public function search($keyword){



        $objHobbiesModel = new Hobbies();

        $searchResult =  $objHobbiesModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("hobbies","LIKE","%$keyword%")
            ->paginate(5);


        return view('Hobbies/search_result',compact('hobbiessearchResult')) ;

    }

}
