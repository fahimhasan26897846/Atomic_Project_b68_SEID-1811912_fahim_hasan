@extends('../master')

@section('title','Summary Of Organization - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> SOP - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Summary_Of_Organization/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('summary','Summary of Organization:') !!}<br>
            {!! Form::textarea('summary', $oneData['summary'], ['size' => '70x5']) !!}
            <br>


     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection