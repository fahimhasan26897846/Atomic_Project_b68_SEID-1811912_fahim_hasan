@extends('../master')


@section('title','Summary of organization - Active List')


@section('content')


    <div class="container">

        <div class="navbar">

            <a href="/summary_of_organization"><button type="button" class="btn btn-primary">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'Summary_Of_Organization/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}




        Total: {!! $allData->total() !!} Summary(s) <br>

        Showing: {!! $allData->count() !!} Summary(s) <br>

        {!! $allData->links() !!}




        <table class="table table-bordered table table-striped" >

            <th>name</th>
            <th>summary</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['name'] !!} </td>
                    <td>  {!! $oneData['summary'] !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>
        {!! $allData->links() !!}
    </div>



@endsection