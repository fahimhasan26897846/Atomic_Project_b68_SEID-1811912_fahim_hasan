@extends('../master')

@section('title','Book Title - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Book Title - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Email/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::label('email','email:') !!}
            {!! Form::email('email',$oneData['email'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection