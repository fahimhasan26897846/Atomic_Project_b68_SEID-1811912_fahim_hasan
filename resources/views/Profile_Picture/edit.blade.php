@extends('../master')

@section('title','PP - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> PP - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Profile_Picture/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('profile_picture','Profile Picture:') !!}
            {!! Form::file('profile_picture') !!}
            <br>

            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection